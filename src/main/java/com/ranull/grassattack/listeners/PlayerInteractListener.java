package com.ranull.grassattack.listeners;

import com.ranull.grassattack.managers.GrassManager;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {
    private GrassManager grassManager;

    public PlayerInteractListener(GrassManager grassManager) {
        this.grassManager = grassManager;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Entity entity = grassManager.rayTraceEntity(player);
        Block block = grassManager.rayTraceBlock(player);

        if (player.getGameMode() != GameMode.SPECTATOR && player.hasPermission("grassattack.use")
                && entity != null && entity instanceof LivingEntity && !entity.hasPermission("grassattack.ignore")
                && block != null && grassManager.getBlockHardness(block) == 0.0) {
            LivingEntity livingEntity = (LivingEntity) entity;

            if (player.getGameMode() != GameMode.ADVENTURE && event.getHand() == EquipmentSlot.HAND
                    && (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)) {
                grassManager.attackEntity(player, livingEntity);
            } else if (grassManager.hasDualWield() && event.getHand() == EquipmentSlot.OFF_HAND
                    && (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
                ItemStack itemInOffHand = player.getInventory().getItemInOffHand();

                if (itemInOffHand.getAmount() != 0 && grassManager.getWieldManager().isValidItem(itemInOffHand)) {
                    grassManager.getWieldManager().attackEntityOffHand(player, livingEntity);
                }
            }
        }
    }
}
