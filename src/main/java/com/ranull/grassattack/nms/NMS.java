package com.ranull.grassattack.nms;

import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public interface NMS {
    void attackEntity(Player player, LivingEntity livingEntity);

    float getBlockHardness(Block block);
}
