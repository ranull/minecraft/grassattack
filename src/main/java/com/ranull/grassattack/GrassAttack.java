package com.ranull.grassattack;

import com.ranull.grassattack.listeners.PlayerAnimationListener;
import com.ranull.grassattack.listeners.PlayerInteractListener;
import com.ranull.grassattack.managers.GrassManager;
import com.ranull.grassattack.nms.*;
import org.bukkit.plugin.java.JavaPlugin;

public class GrassAttack extends JavaPlugin {
    private NMS nms;

    @Override
    public void onEnable() {
        if (setupNMS()) {
            GrassManager grassManager = new GrassManager(this, nms);

            getServer().getPluginManager().registerEvents(new PlayerAnimationListener(grassManager), this);
            getServer().getPluginManager().registerEvents(new PlayerInteractListener(grassManager), this);
        } else {
            getLogger().severe("Version not supported, disabling plugin!");
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    private boolean setupNMS() {
        try {
            String version = getServer().getClass().getPackage().getName().split("\\.")[3];

            if (version.equals("v1_13_R1")) {
                nms = new NMS_v1_13_R1();
            } else if (version.equals("v1_13_R2")) {
                nms = new NMS_v1_13_R2();
            } else if (version.equals("v1_14_R1")) {
                nms = new NMS_v1_14_R1();
            } else if (version.equals("v1_15_R1")) {
                nms = new NMS_v1_15_R1();
            } else if (version.equals("v1_16_R1")) {
                nms = new NMS_v1_16_R1();
            } else if (version.equals("v1_16_R2")) {
                nms = new NMS_v1_16_R2();
            } else if (version.equals("v1_16_R3")) {
                nms = new NMS_v1_16_R3();
            }

            return nms != null;
        } catch (ArrayIndexOutOfBoundsException ignored) {
            return false;
        }
    }
}
