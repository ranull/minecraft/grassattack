package com.ranull.grassattack.listeners;

import com.ranull.grassattack.managers.GrassManager;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;

public class PlayerAnimationListener implements Listener {
    private GrassManager grassManager;

    public PlayerAnimationListener(GrassManager grassManager) {
        this.grassManager = grassManager;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerAnimation(PlayerAnimationEvent event) {
        Player player = event.getPlayer();

        if (event.getAnimationType() == PlayerAnimationType.ARM_SWING
                && player.getGameMode() == GameMode.ADVENTURE && player.hasPermission("grassattack.use")) {
            Entity entity = grassManager.rayTraceEntity(player);
            Block block = grassManager.rayTraceBlock(player);

            if (entity != null && entity instanceof LivingEntity && !entity.hasPermission("grassattack.ignore")
                    && block != null && grassManager.getBlockHardness(block) == 0.0) {
                grassManager.attackEntity(player, (LivingEntity) entity);
            }
        }
    }
}
