package com.ranull.grassattack.managers;

import com.ranull.dualwield.DualWield;
import com.ranull.dualwield.managers.WieldManager;
import com.ranull.grassattack.GrassAttack;
import com.ranull.grassattack.nms.NMS;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.RayTraceResult;

public class GrassManager {
    private NMS nms;
    private GrassAttack plugin;
    private double range = 5;

    public GrassManager(GrassAttack plugin, NMS nms) {
        this.plugin = plugin;
        this.nms = nms;
    }

    public Block rayTraceBlock(Player player) {
        RayTraceResult rayTraceResult = player.getWorld().rayTraceBlocks(player.getEyeLocation(),
                player.getEyeLocation().getDirection(), range);

        return rayTraceResult != null && rayTraceResult.getHitBlock() != null ? rayTraceResult.getHitBlock() : null;
    }

    public Entity rayTraceEntity(Player player) {
        RayTraceResult rayTraceResult = player.getWorld().rayTraceEntities(player.getEyeLocation(),
                player.getEyeLocation().getDirection(), range, entity -> !(entity == player));

        return rayTraceResult != null && rayTraceResult.getHitEntity() != null ? rayTraceResult.getHitEntity() : null;
    }

    public double getBlockHardness(Block block) {
        return nms.getBlockHardness(block);
    }

    public void attackEntity(Player player, LivingEntity livingEntity) {
        nms.attackEntity(player, livingEntity);
    }

    public boolean hasDualWield() {
        Plugin dualWield = plugin.getServer().getPluginManager().getPlugin("DualWield");

        return dualWield != null && dualWield.isEnabled();
    }

    public WieldManager getWieldManager() {
        return ((DualWield) (plugin.getServer().getPluginManager().getPlugin("DualWield")))
                .getDualWieldAPI().getWieldManager();
    }
}
