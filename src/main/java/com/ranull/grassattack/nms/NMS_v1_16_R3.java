package com.ranull.grassattack.nms;

import net.minecraft.server.v1_16_R3.*;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class NMS_v1_16_R3 implements NMS {
    @Override
    public void attackEntity(Player player, LivingEntity livingEntity) {
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
        Entity nmsEntity = ((CraftEntity) livingEntity).getHandle();

        entityPlayer.attack(nmsEntity);
    }

    @Override
    public float getBlockHardness(org.bukkit.block.Block block) {
        World nmsWorld = ((CraftWorld) block.getWorld()).getHandle();
        Block nmsBlock = nmsWorld.getType(new BlockPosition(block.getX(), block.getY(), block.getZ())).getBlock();

        return nmsBlock.getBlockData().strength;
    }
}
